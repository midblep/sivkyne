module.exports = {
	darkMode: 'class',
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		fontFamily: {
			body: 'Roboto, sans-serif'
		},
		container: {
			center: true,
			screens: {
				sm: '100%',
				md: '100%',
				lg: '700px',
				xl: '1000px'
			},
			borderWidth: {
				DEFAULT: '1px',
				0: '0',
				2: '2px',
				3: '3px',
				4: '4px',
				6: '6px',
				8: '8px',
				12: '12px',
				20: '20px'
			}
		},
		extend: {
			blur: {
				xs: '2px'
			},
			backgroundOpacity: {
				1: '.01'
			}
		}
	},
	plugins: []
};
